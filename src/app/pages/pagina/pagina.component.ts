import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  public nombre: string;
  public apellidos: string;
  public fullname: string;

  constructor(private activeRoute: ActivatedRoute, private _router: Router) {
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {

      this.nombre = params.nombre +' ';
      this.apellidos = params.apellidos;
      this.fullname = this.nombre + this.apellidos
    });
  }

  redireccion(){
    this._router.navigate(['pagina','Rafael','Rebolledo'])
  }

}
