import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Article } from '../models/article';
import { UrlGlobal } from '../services/global';

@Injectable()
export class ArticleService {

  public url: string;

  constructor(private _http: HttpClient) {
    this.url = UrlGlobal;
  }

  getArticles(last: any = null): Observable<any> {
    let articles = "articles"
    if (last != null) {
      articles = "articles/true";
    }
    return this._http.get(`${this.url}/${articles}`);
  }
  getArticle(id: any): Observable<any> {
    return this._http.get(`${this.url}/article/${id}`);
  }
  search(search: string): Observable<any> {
    return this._http.get(`${this.url}/search/${search}`);
  }
  guardarArticle(article): Observable<any> {
    let params = JSON.stringify(article);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(`${this.url}/save`, params, { headers });
  }
  actualizarArticle(id,article):Observable<any>{
    let params=JSON.stringify(article);
    let headers=new HttpHeaders().set('Content-Type','application/json');
    return this._http.put(`${this.url}/article/${id}`, params, { headers });
  }
  deleteArticle(id):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json');
    return this._http.delete(`${this.url}/article/${id}`, { headers });

  }


  prueba() {
    console.log(this.url);
  }
}
