import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article';
import { UrlGlobal } from 'src/app/services/global';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ArticleService]

})
export class HomeComponent implements OnInit {

  public articles:Article[];

  constructor(private _articleService:ArticleService) { }

  ngOnInit(): void {
    this._articleService.getArticles(true).subscribe(resp => {
      this.articles = resp.articles
      // console.log(this.articles);
    },
      error => {
        console.log(error);
      });
  }

}
