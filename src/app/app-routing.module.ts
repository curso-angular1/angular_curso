import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './pages/home/home.component';
import { BlogComponent } from './pages/blog/blog.component';
import { FormularioComponent } from './pages/formulario/formulario.component';
import { PaginaComponent } from './pages/pagina/pagina.component';
import { PeliculasComponent } from './pages/peliculas/peliculas.component';
import { ErrorComponent } from './pages/error/error.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
import { SearchComponent } from './components/search/search.component';
import { CrearArticuloComponent } from './components/crear-articulo/crear-articulo.component';
import { ArticleEditComponent } from './components/article-edit/article-edit.component';

//Array de routes

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'formulario', component: FormularioComponent },
  { path: 'peliculas', component: PeliculasComponent },
  { path: 'pagina', component: PaginaComponent },
  { path: 'pagina/:nombre/:apellidos', component: PaginaComponent },
  { path: 'blog/article/:id', component: ArticleDetailComponent },
  { path: 'buscar/:search', component: SearchComponent },
  { path: 'blog/crear', component: CrearArticuloComponent },
  { path: 'blog/editar/:id', component: ArticleEditComponent },

  { path: '**', component: ErrorComponent }
]

export const appRoutingProvider: any = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);



