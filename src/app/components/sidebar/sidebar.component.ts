import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public campoBusqueda: any;
  constructor(
    private _router: Router,
    private _activatedRouter: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }
  buscar() {
    if (this.campoBusqueda === '') {
      alert('escribe algo imbecil')
    } else {
      this._router.navigate(['/buscar',this.campoBusqueda]);
    }

  }

}
