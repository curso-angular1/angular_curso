import { Injectable } from '@angular/core';
import { Pelicula } from '../models/pelicula';



@Injectable()
export class PeliculaService {

  public peliculas: Pelicula[];


  holamundo() {
    return 'hola mundo desde el servicio de angular';
  }
  getPeliculas() {
    this.peliculas = [
      new Pelicula('iroman', 'https://is1-ssl.mzstatic.com/image/thumb/Video128/v4/89/74/cf/8974cfa0-5e27-1c5e-390a-e97e5d12a51d/contsched.rdzrzprk.lsr/268x0w.jpg', 2019),
      new Pelicula('avengers', 'https://www.infobae.com/new-resizer/PRx_y_Dg0OEQ9CgpBXo_r7M5yUQ=/750x0/filters:quality(100)/s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2019/03/14113157/marvel-end-1.jpg', 2020),
      new Pelicula('black panther', 'https://laverdadnoticias.com/__export/1518795639677/sites/laverdad/img/2018/02/16/item-10796-7a31c789-861d-4522-a779-ce4523d3b463.jpg_478486366.jpg', 2021),
    ];
    return this.peliculas;
  }
}

