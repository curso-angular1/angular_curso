import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Pelicula } from '../../models/pelicula';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  @Input() item: Pelicula;
  @Input() i: number;
  @Output() MarcarFavorito = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  seleccionar(event, pelicula) {
    this.MarcarFavorito.emit({
      pelicula
    })
    // console.log(pelicula);
  }

}
