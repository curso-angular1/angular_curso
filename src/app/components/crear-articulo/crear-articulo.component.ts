import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UrlGlobal } from '../../services/global';
import swal from 'sweetalert';

@Component({
  selector: 'app-crear-articulo',
  templateUrl: './crear-articulo.component.html',
  styleUrls: ['./crear-articulo.component.css'],
  providers: [ArticleService]
})
export class CrearArticuloComponent implements OnInit {

  public article: Article;
  public status: string;
  public titulo:string;

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "1",
    uploadAPI: {
      url:`${UrlGlobal}/upload-image` ,
      // headers: {
      //   "Content-Type": "text/plain;charset=UTF-8",
      //   "Authorization": `Bearer ${token}`
      // }
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube tu imagen para el articulo',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
  };

  constructor(
    private _articleService: ArticleService,
    private _router: Router,
    private _activatedRouter: ActivatedRoute) {
    this.article = new Article('', '', '', null, null);
    this.titulo="Crear articulo";

  }

  ngOnInit(): void {
  }
  onSubmit() {
    this._articleService.guardarArticle(this.article).subscribe(resp => {
      if (resp.status === 'success') {
        this.status = "success";
        this.article = resp.article;
        swal("Articulo creado!", "El articulo ha sido creado correctamente!", "success");

        this._router.navigateByUrl('/blog');
      } else {
        swal("Articulo no creado!", "El articulo no se ha creado!", "error");

        this.status = 'error'
      }

    }, error => {
      console.log(error);
      this.status = 'error';
    })

  }
  imageUpload(data){
    let image_data=JSON.parse(data.response);
    this.article.image=image_data.image;
  }

}
