import { Component, OnInit } from '@angular/core'
import { Pelicula } from '../../models/pelicula';
import { PeliculaService } from '../../services/pelicula.service'

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css'],
  providers: [PeliculaService]
})
export class PeliculasComponent implements OnInit {

  public peliculas: Pelicula[];
  public favorito: Pelicula;
  public fecha: Date;

  constructor(private _peliculaService: PeliculaService) {
    this.peliculas = _peliculaService.getPeliculas()
    this.fecha = new Date(2020, 8, 12)
  }

  ngOnInit(): void {
    console.log(this._peliculaService.holamundo());

  }
  pMostrarFavorito(event) {
    this.favorito = event.pelicula
  }

}
