import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../models/article';
import { UrlGlobal } from 'src/app/services/global';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() articles: Article[]
  public url: string;
  constructor() {
    this.url = UrlGlobal;
  }

  ngOnInit(): void {
  }

}
