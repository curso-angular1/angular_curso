import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service'
import { Article } from '../../models/article';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UrlGlobal } from '../../services/global';


@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css'],
  providers: [ArticleService]
})
export class ArticleDetailComponent implements OnInit {


  public article: Article;
  public url: string
  constructor(
    private _articleService: ArticleService,
    private _activatedRouted: ActivatedRoute,
    private _router: Router
  ) {

    this.url = UrlGlobal
  }

  ngOnInit(): void {
    this._activatedRouted.params.subscribe(params => {
      let id = params['id'];
      this._articleService.getArticle(id).subscribe(resp => {
        if (!resp.article) {
          this._router.navigateByUrl('/');
        }
        this.article = resp.article;
        // console.log(this.article);
      }, error => {
        this._router.navigateByUrl('/');
      })
    })
  }
  deleteArticle(id) {
    this._articleService.deleteArticle(id).subscribe(resp => {
      this._router.navigateByUrl('/blog')
    }, error => {
      console.log(error);
      this._router.navigateByUrl('/blog')

    })
  }

}
