import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UrlGlobal } from '../../services/global';
import swal from 'sweetalert';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css'],
  providers: [ArticleService]
})
export class ArticleEditComponent implements OnInit {

  public article: Article;
  public status: string;
  public isedit: boolean;
  public titulo: string;
  public url:string;


  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "1",
    uploadAPI: {
      url: `${UrlGlobal}/upload-image`,
      // headers: {
      //   "Content-Type": "text/plain;charset=UTF-8",
      //   "Authorization": `Bearer ${token}`
      // }
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube tu imagen para el articulo',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
  };
  constructor(
    private _articleService: ArticleService,
    private _router: Router,
    private _activatedRouter: ActivatedRoute) {
    this.article = new Article('', '', '', null, null);
    this.isedit = true;
    this.titulo = "Editar articulo";
    this.url=UrlGlobal;
  }

  ngOnInit(): void {
    this.getArticle();
  }

  onSubmit() {
    this._articleService.actualizarArticle(this.article._id,this.article).subscribe(resp => {
      if (resp.status === 'success') {
        this.status = "success";
        this.article = resp.article;
        swal("Articulo editado!", "El articulo ha sido editado correctamente!", "success");

        this._router.navigateByUrl(`/blog/article/${this.article._id}`);
      } else {
        swal("Articulo no editado!", "El articulo no se ha editado!", "error");

        this.status = 'error'
      }

    }, error => {
      console.log(error);
      this.status = 'error';
    })

  }
  imageUpload(data) {
    let image_data = JSON.parse(data.response);
    this.article.image = image_data.image;
  }
  getArticle() {
    this._activatedRouter.params.subscribe(params => {
      let id = params['id'];
      this._articleService.getArticle(id).subscribe(resp => {
        if (!resp.article) {
          this._router.navigateByUrl('/');
        }
        this.article = resp.article;
        // console.log(this.article);
      }, error => {
        this._router.navigateByUrl('/');
      })
    })

  }

}

