import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [ArticleService]
})
export class SearchComponent implements OnInit {

  public articles: Article[];
  public busqueda: string;

  constructor(
    private _articleService: ArticleService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(params => {
      let search = params['search'];
      this.busqueda = search
      this._articleService.search(search).subscribe(resp => {
        if (resp.articles) {

          this.articles = resp.articles;
        }
      }, error => {
        // this._router.navigateByUrl('/')
        console.log(error);
      })
    })


  }

}
