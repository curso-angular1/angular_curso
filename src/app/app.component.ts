import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public homeText = 'Bienvenido al Curso de Angular con Víctor Robles de victorroblesweb.es';
}
