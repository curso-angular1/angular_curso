import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'espar'
})

export class esparPipe implements PipeTransform {
  transform(value: any) {

    let espar = false;
    if (value % 2 == 0) {
      espar = true;
    }
    return `El año es ${value} ${espar}`;
  }
}
